var pjson = require('./package.json');
var config = require('./configs.js');

module.exports = function(grunt) {

    grunt.initConfig({

        // Sass
        sass: {
            dist: {
                files: {
                    'assets/css/style.min.css' : 'assets/_scss/main.scss'
                },
                options: {
                    style: 'compressed',
                    sourcemap: config.debug == true ? 'auto' : 'none'
                }
            }
        },

        // Uglify
        uglify: {
            build: {
                src: 'assets/js/app.js',
                dest: 'assets/js/app.min.js'
            }
        },

        jshint: {
            options: {
                browser: true,
                globals: {
                    jQuery: true
                },
            },
            all: ['assets/js/app.js']
        },

        // AutoPrefixer
        autoprefixer: {
            options: {
                browsers: ['last 100 versions'],
                map: true
            },
            dist: {
                files:{
                  'assets/css/style.min.css':'assets/css/style.min.css'
                }
            }
        },

        //Watch files for task run
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass', 'autoprefixer']
            },
            js: {
                files: 'assets/js/app.js',
                tasks: ['uglify', 'jshint']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');

    grunt.registerTask('default', ['watch'], function() {
        console.log( '\x1b[31m\x1b[1mHi '+ pjson.author +':\x1b[22m \x1b[93m everything is\x1b[4m\x1b[24m ok! \x1b[0m' );
    });
}
