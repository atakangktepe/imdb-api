var app = angular.module('imdbApi', []);

app.controller('MainCtrl', ['$scope','$http', function ($scope, $http) {
    $scope.searchInputValue = '';
    $scope.searchValue = '';

    $scope.updateValue = function (searchInputValue) {
        $scope.searchValue = angular.copy($scope.searchInputValue);
        $scope.searchUrl = 'http://www.omdbapi.com/?t=' + $scope.searchValue;
    };

    $scope.search = function () {
        $scope.updateValue();
        $scope.searching = true;

        $http.get($scope.searchUrl).
            success(function(data) {
                $scope.apiGet = data;
            });
    };
}]);